# Axe Bar Website

###### (nom temporaire)

## Lancer le projet

Aller à la racine du projet puis :

```shell
docker-compose up --build
```

## Structure du projet

### Authentification

#### Privilèges

le site possède un système d'utilisateurs à privilèges, qui sont au nombre de 5 :

* Créer/Supprimer/modifier les Evènements
* Modifier les privilèges des autres utilisateurs (impossible de donner des droits que l'on ne possède pas)
* Bannir des utilisateurs

#### Compte Root

Il existe un compte root qui possède tous les privilèges et ne peut pas les perdre.

```Json
email: root@root.fr
password: 12345
```