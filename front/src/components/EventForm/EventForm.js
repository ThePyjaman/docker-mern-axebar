import React, {useEffect, useState} from "react";
import {Button, Paper, TextField, Typography} from "@material-ui/core";
import useStyles from './styles'
import FileBase from 'react-file-base64';
import {useDispatch, useSelector} from "react-redux";
import {createEvent, updateEvent} from "../../Redux/actions/events";
import {useHistory} from 'react-router-dom';


const EventForm = (props) => {


    console.log(props)
    const history = useHistory()


    const [eventData, setEventData] = useState({
        title: '',
        message: '',
        link: '',
        selectedFile: '',
    });

    const [formTitle, setFormTitle] = useState({
        title: '',
    });


    const classes = useStyles();
    const dispatch = useDispatch()

    const events = useSelector((state) => state.events)


    useEffect(() => {
        const event = events.find(x => x._id)
        if (props.id && event) {
            setEventData(event)
            setFormTitle(event)
        }
    }, [events, props])

    const handleSubmit = (e) => {
        e.preventDefault();
        if (props.id) {
            dispatch(updateEvent(props.id, eventData))
            history.push('/event-list')
            console.log('updateEvent')
        } else {
            dispatch(createEvent(eventData));
            history.push('/')
            console.log("createEvent")
        }
        clear();
    }

    const clear = () => {
        setEventData({
            title: '',
            message: '',
            link: '',
            selectedFile: '',
        });
    }

    const backToEventList = () => {
        history.push('/event-list')
    }
    return (
        <Paper className={classes.paper}>
            <form autoComplete='off' noValidate className={`${classes.root} ${classes.form}`} onSubmit={handleSubmit}>
                <Typography variant="h6">{props.id ? `Editing "${formTitle.title}"` : 'Creating an Event'}</Typography>
                <TextField
                    name="title"
                    variant="outlined"
                    label='Event title'
                    fullWidth
                    value={eventData.title}
                    onChange={(e) => setEventData({...eventData, title: e.target.value})}
                />
                <TextField
                    name="message"
                    variant="outlined"
                    label='Content'
                    fullWidth
                    value={eventData.message}
                    onChange={(e) => setEventData({...eventData, message: e.target.value})}
                />
                <TextField
                    name="link"
                    variant="outlined"
                    label='Link'
                    fullWidth
                    value={eventData.link}
                    onChange={(e) => setEventData({...eventData, link: e.target.value})}
                />
                <div className={classes.fileInput}>
                    <FileBase
                        type='file'
                        multiple={false}
                        onDone={({base64}) => setEventData({...eventData, selectedFile: base64})}
                    >
                    </FileBase>
                </div>
                <Button
                    className={classes.formButton}
                    variant='contained'
                    color="primary"
                    size='large'
                    type='submit'
                    fullWidth
                >
                    Submit
                </Button>
                <Button
                    className={classes.formButton}
                    variant='contained'
                    color="secondary"
                    size='small'
                    onClick={clear}
                    fullWidth
                >
                    Clear
                </Button>
                <Button
                    className={classes.formButton}
                    variant='contained'
                    color="secondary"
                    size='small'
                    onClick={backToEventList}
                    fullWidth
                >
                    Go to Event List
                </Button>
            </form>

        </Paper>
    )
}
export default EventForm