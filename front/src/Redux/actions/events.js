import * as api from '../api';
import {CREATE, DELETE, FETCH_ALL, FETCH_ONE, UPDATE} from "../../assets/constants/actionTypes";

// Action Creators

export const getEvents = () => async (dispatch) => {

    try {
        const {data} = await api.fetchEvents()

        return dispatch({type: FETCH_ALL, payload: data});
    } catch (error) {
        console.log(error.message)
    }
}

export const createEvent = (event) => async (dispatch) => {

    try {
        const {data} = await api.createEvent(event);

        dispatch({type: CREATE, payload: data})
    } catch (error) {
        console.log(error.message)
    }
}

export const updateEvent = (id, post) => async (dispatch) => {
    try {
        const {data} = await api.updateEvent(id, post);

        dispatch({type: UPDATE, payload: data});
    } catch (error) {
        console.log(error.message);
    }
}

export const getEvent = (id) => async (dispatch) => {

    try {
        const {data} = await api.fetchEvent(id)

        return dispatch({type: FETCH_ONE, payload: data});
    } catch (error) {
        console.log(error.message)
    }
}


export const deleteEvent = (id) => async (dispatch) => {
    try {
        await api.deleteEvent(id);

        dispatch({type: DELETE, payload: id})
    } catch (error) {
        console.log(error)
    }
}