import React, {useState} from "react"
import {Drawer, IconButton, List, ListItem, ListItemText} from "@material-ui/core"
import {Menu} from "@material-ui/icons"
import useStyles from "./styles";
import {useHistory} from "react-router-dom";

const SideDrawer = () => {

    const history = useHistory()
    const classes = useStyles();

    const [state, setState] = useState({left: false}) // Add this
    const toggleDrawer = (anchor, open) => event => {
        if (
            event.type === "keydown" &&
            (event.key === "Tab" || event.key === "Shift")
        ) {
            return
        }
        setState({[anchor]: open})
    }

    const sideDrawerList = anchor => (
        <div
            className={classes.list} /*Add this */
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
        >
            <List component="nav">
                <ListItem button={true} onClick={() => {
                    history.push('/')
                }} className={classes.linkText}>
                    <ListItemText primary="Accueil"/>
                </ListItem>
                <ListItem button={true} onClick={() => {
                    history.push('/presentation')
                }} className={classes.linkText}>
                    <ListItemText primary="Présentation"/>
                </ListItem>
                <ListItem button={true} onClick={() => {
                    history.push('/food')
                }} className={classes.linkText}>
                    <ListItemText primary="Bouffe"/>
                </ListItem>
                <ListItem button={true} onClick={() => {
                    history.push('/calendar')
                }} className={classes.linkText}>
                    <ListItemText primary="Calendrier"/>
                </ListItem>
                <ListItem button={true} onClick={() => {
                    history.push('/reservation')
                }} className={classes.linkText}>
                    <ListItemText primary="Réservation"/>
                </ListItem>
            </List>
        </div>
    )

    return (
        <React.Fragment>
            <IconButton
                edge="start"
                aria-label="menu"
                onClick={toggleDrawer("left", true)}
            >
                <Menu fontSize="large" style={{color: `white`}}/>
            </IconButton>
            <Drawer
                anchor="left"
                open={state.left}
                onOpen={toggleDrawer("left", true)}
                onClose={toggleDrawer("left", false)}
            >
                {sideDrawerList("left")}
            </Drawer>
        </React.Fragment>
    )
}
export default SideDrawer

