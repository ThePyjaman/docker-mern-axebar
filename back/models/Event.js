import mongoose from "mongoose";

const eventSchema = mongoose.Schema({
    title: String,
    message: String,
    link: String,
    selectedFile: String,
    createdAt: {
        type: Date,
        default: new Date()
    }
})

const Event = mongoose.model('Event', eventSchema)
export default Event;