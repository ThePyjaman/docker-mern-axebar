import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import {AccountCircle, Home} from "@material-ui/icons";
import {useHistory} from "react-router-dom";
import {Hidden, List, ListItem, ListItemText, Menu, MenuItem} from "@material-ui/core";
import useStyles from "./styles";
import SideDrawer from "./SideDrawer/SideDrawer";


export default function NavigationBar() {

    const auth = 1;
    const classes = useStyles();
    const history = useHistory()
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Hidden mdUp>
                        <SideDrawer/>
                    </Hidden>
                    <Hidden smDown>
                        <List component="nav" aria-labelledby="main navigation" className={classes.navDisplayFlex}>
                            <IconButton
                                edge="start"
                                color="inherit"
                                aria-label="home"
                                onClick={() => {
                                    history.push('/')
                                }}
                            >
                                <Home fontSize="large"/>
                            </IconButton>
                            <ListItem button={true} onClick={() => {
                                history.push('/presentation')
                            }} className={classes.linkText}>
                                <ListItemText primary="Présentation" className={classes.itemText}/>
                            </ListItem>
                            <ListItem button={true} onClick={() => {
                                history.push('/food')
                            }} className={classes.linkText}>
                                <ListItemText primary="Bouffe" className={classes.itemText}/>
                            </ListItem>
                            <ListItem button={true} onClick={() => {
                                history.push('/calendar')
                            }} className={classes.linkText}>
                                <ListItemText primary="Calendrier" className={classes.itemText}/>
                            </ListItem>
                            <ListItem button={true} onClick={() => {
                                history.push('/reservation')
                            }} className={classes.linkText}>
                                <ListItemText primary="Réservation" className={classes.itemText}/>
                            </ListItem>
                        </List>
                    </Hidden>
                    <div className={classes.toolbarButtons}>
                        {auth && (
                            <div>
                                <IconButton
                                    aria-label="account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    onClick={handleMenu}
                                    color="inherit"
                                >
                                    <AccountCircle/>
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={open}
                                    onClose={handleClose}
                                >
                                    <MenuItem onClick={handleClose}>Profile</MenuItem>
                                    <MenuItem onClick={handleClose}>My account</MenuItem>
                                </Menu>
                            </div>
                        )}
                    </div>
                </Toolbar>
            </AppBar>
        </div>
    )
}
