import React from "react";
import EventForm from "../../components/EventForm/EventForm";
import {useParams} from "react-router";


const EventFormPage = () => {

    const {id} = useParams()
    return (
        <EventForm id={id}/>
    )
}
export default EventFormPage;